# Landing Page

Author: Alex McNeill
Date: 11th March 2017

## About

Simple landing page project that highlights the two core cat enclosures of the business.

Mocked this up using preprocessed languages like Stylus and Pugjs with a tool called Prepros.

## Live demo:

The following repo is available as a page at https://spctrldst.gitlab.io/cat-enclosures-demo/dist/html/landing.html

## Missing features:

- animated scrolling
- return to top button that fades in after scrolling past the fold
- hamburger menu that replaces the top nav menu when on a mobile device
- stock images that are blurred for some of the sections that are royalty free
- optimization of the values section for smaller screens
- newsletter sign up to generate leads
- sitemap content to encourage users to explore the site further after reading the landing page

